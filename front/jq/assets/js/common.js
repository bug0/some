var storage = {
    isSession: function () {
        if (window.sessionStorage) {
            return true;
        } else {
            alert("浏览器不支持sessionStorage");
            return false;
        }
    },
    isLocal: function () {
        if (window.localStorage) {
            return true;
        } else {
            alert("浏览器不支持localStorage");
            return false;
        }
    },
    sets: function (key, value) {
        if (this.isSession()) {
            window.sessionStorage.setItem(key, JSON.stringify(value));
        }
    },
    gets: function (key) {
        if (this.isSession()) {
            return JSON.stringify(window.sessionStorage.getItem(key));
        }
    },
    removes: function (key) {
        if (this.isSession()) {
            window.sessionStorage.removeItem(key);
        }
    },
    clears: function () {
        if (this.isSession()) {
            window.sessionStorage.clear();
        }
    },
    setl: function (key, value) {
        if (this.isLocal()) {
            window.localStorage.setItem(key, JSON.stringify(value));
        }
    },
    getl: function (key) {
        if (this.isLocal()) {
            return JSON.stringify(window.localStorage.getItem(key));
        }
    },
    removel: function (key) {
        if (this.isLocal()) {
            window.localStorage.removeItem(key);
        }
    },
    clearl: function () {
        if (this.isLocal()) {
            window.localStorage.clear();
        }
    }
}
function getRequest(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r != null) return unescape(r[2]);
    return null;
};
function getUrlPar(param) { //param为要获取的参数名 注:获取不到是为null
    var currentUrl = window.location.href; //获取当前链接
    var arr = currentUrl.split("?");//分割域名和参数界限
    if (arr.length < 2) {
        return null;
    }
    arr = arr[1].split("&");//分割参数
    for (var i = 0; i < arr.length; i++) {
        var tem = arr[i].split("="); //分割参数名和参数内容
        if (tem[0] == param) {
            return tem[1];
        }
    }
    return null;
}