package com.xzc.one.generate;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * 逆向工程
 *
 * @author xzcong
 * @create 2019-10-22 19:54
 **/
class MybatisPlusGenerator {
    /**
     * MySQL 生成演示
     */
    public static void main(String[] args) {
        AutoGenerator autoGenerator = new AutoGenerator();
        System.out.println("开始执行……");
        //全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir("D:/work/src/main");
        globalConfig.setFileOverride(true);//是否覆盖
        globalConfig.setActiveRecord(true);
        globalConfig.setEnableCache(false);// XML 二级缓存
        globalConfig.setBaseResultMap(true);// XML ResultMap
        globalConfig.setBaseColumnList(false);// XML columList
        globalConfig.setAuthor("xzcong");
        //生成文件名:
        globalConfig.setXmlName("%sMapper");
        globalConfig.setMapperName("%sMapper");
//        globalConfig.setServiceName("%sService");
//        globalConfig.setServiceImplName("%sImpl");
        globalConfig.setServiceName("%sInterface");
        globalConfig.setServiceImplName("%sService");
        globalConfig.setControllerName("%sController");

        autoGenerator.setGlobalConfig(globalConfig);

        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL);
//        name: share_vr
//        driver-class-name: com.mysql.jdbc.Driver
//        url: jdbc:mysql://192.168.0.7:3306/share_vr?characterEncoding=UTF-8&useSSL=true
//        username: root
//        password: ith.666
        dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("ith.666");
        dataSourceConfig.setUrl("jdbc:mysql://192.168.0.7:3306/share_vr?characterEncoding=UTF-8&useSSL=true");
        autoGenerator.setDataSource(dataSourceConfig);

        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
//        strategyConfig.setCapitalMode(true);    // 全局大写命名 ORACLE 注意
        strategyConfig.setTablePrefix(new String[]{"t_drp_", "qrtz_", "t_sys"});// 此处可以修改为您的表前缀
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);//实体是否为lombok模型（默认 false）
        //strategyConfig.setInclude(new String[]{"sys_user"}); // 需要生成的表
        //strategyConfig.setExclude(new String[]{"test"}); // 排除生成的表
        strategyConfig.setExclude();
        // strategyConfig.setSuperEntityClass("com.baomidou.demo.TestEntity");// 自定义实体父类
        // strategyConfig.setSuperEntityColumns(new String[] { "test_id", "age" });// 自定义实体，公共字段
        // strategyConfig.setSuperMapperClass("com.baomidou.demo.TestMapper"); // 自定义 mapper 父类
        // strategyConfig.setSuperServiceClass("com.baomidou.demo.TestService");// 自定义 service 父类
        // strategyConfig.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");// 自定义 service 实现类父类
        // strategyConfig.setSuperControllerClass("core.system.module.controller.IBaseController");// 自定义 controller 父类
        autoGenerator.setStrategy(strategyConfig);

        // 包配置
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(null);
        packageConfig.setEntity("java.com.xzc.model");
        packageConfig.setMapper("java.com.xzc.mapper.mapper");
        packageConfig.setXml("java.com.xzc.mapper.xml");
        packageConfig.setController("java.com.xzc.controller");
        packageConfig.setService("java.com.xzc.service.interface");
        packageConfig.setServiceImpl("java.com.xzc.service.implement");
        //ackageConfig.setXml("resources.mapper");
        autoGenerator.setPackageInfo(packageConfig);

        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-完成");
                this.setMap(map);
            }
        };
        autoGenerator.setCfg(injectionConfig);

        // 执行生成
        autoGenerator.execute();

        // 打印注入设置【可无】
        System.err.println(autoGenerator.getCfg().getMap().get("abc"));
    }
}
