package com.xzc.one.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xzc.one.mapper.SysUserMapper;
import com.xzc.one.model.SysUser;
import com.xzc.one.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-23
 */
@Service
public class SysUserImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
