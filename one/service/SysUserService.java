package com.xzc.one.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xzc.one.model.SysUser;

/**
 * <p>
 * 后台用户 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-23
 */
public interface SysUserService extends IService<SysUser> {

}
