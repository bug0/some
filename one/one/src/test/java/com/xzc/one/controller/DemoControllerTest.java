package com.xzc.one.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xzc.one.App;
import com.xzc.one.business.mapper.mapper.SysUserMapper;
import com.xzc.one.business.model.SysUser;
import com.xzc.one.common.inout.Pager;
import com.xzc.one.common.util.ToolUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoControllerTest {
    @Autowired
    @Resource
    private SysUserMapper sysUserMapper;
    @Autowired
    private TestRestTemplate testRestTemplate;
    @Resource
    private WebApplicationContext webApplicationContext;

    //Application.class 为SpringBoot的启动入口类 都要配置
    @Test
    public void enumDemo() {
        System.out.println("DemoControllerTest.enumDemo");
        Pager pager = new Pager();
        QueryWrapper queryWrapper = new QueryWrapper<>();
        IPage<SysUser> userIPage = sysUserMapper.selectPage(pager, queryWrapper);
        ToolUtil.PrintUtil.print(userIPage);
    }

    @Test
    public void printParam() {
        System.out.println("DemoControllerTest.printParam");
        MockMvc build = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
}