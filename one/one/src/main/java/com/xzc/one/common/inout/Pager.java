package com.xzc.one.common.inout;

import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.*;

/**
 * @author xiongzhicong
 * @create 2019-11-06 17:12
 **/
public class Pager extends Page {
    //  "records": []
//  "total": 2,
//  "size": 5,
//  "current": 1,
//  "pages": 1

    public Pager() {
        super.setCurrent(1);
        super.setSize(20);
    }

    public Pager(int current, int size) {
        super(current, size);
    }

    public Pager(Map<String, Object> pageMap) {
        int current = 1;
        int size = 20;
        if (pageMap.get("current") != null) {
            super.setCurrent(Integer.parseInt((String) pageMap.get("current")));
        }
        if (pageMap.get("size") != null) {
            super.setSize(Integer.parseInt((String) pageMap.get("size")));
        }
        super.setCurrent(current);
        super.setSize(size);
    }
}
