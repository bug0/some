package com.xzc.one.common.util;

import com.xzc.one.common.exception.GlobalException;
import com.xzc.one.common.inout.CodeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Component
public class EmailUtil {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String username;

    @PostConstruct
    public void printFilename() {
        System.out.println(username);
    }

    public String registerTemplate(String text) {
        text = String.format("用户注册\n" +
                "\n" +
                "\n" +
                "这封信是由 xzc 发送的。\n" +
                "\n" +
                "您收到这封邮件，是由于在 xzc网站 获取了新用户注册地址使用了这个邮箱地址。如果您并没有访问过 xzc，或没有进行上述操作，请忽略这封邮件。\n" +
                "\n" +
                "\n" +
                "----------------------------------------------------------------------\n" +
                "新用户注册说明\n" +
                "----------------------------------------------------------------------\n" +
                "\n" +
                "\n" +
                "如果您是 xzc 的新用户，或在修改您的注册 Email 时使用了本地址，我们需要对您的地址有效性进行验证以避免垃圾邮件或地址被滥用。\n" +
                "\n" +
                "您只需点击下面的链接即可进行用户注册，以下链接有效期为1天。过期可以重新请求发送一封新的邮件验证：\n" +
                "%s \n" +
                "\n" +
                "感谢您的访问，祝您使用愉快！\n" +
                "\n" +
                "此致\n" +
                "xzc 管理团队.\n" +
                "http://www.xzc.com/\n", text);
        return text;
    }

    public Email registerTemplate(String to, String text) {
        text = String.format("<h1 style='text-align:center'>用户注册</h1>" +
                "<p style='text-align:center'>欢迎您注册本网站，点击下面链接完成注册(1天有效时间)</p>" +
                "<p style='text-align:center'><a href='%s'>点击完成注册</a></p>", text);
        return new Email(to, "用户注册", text);
    }

    public Email verificationTemplate(String to, String text) {
        text = String.format("<h1 style='text-align:center'>验证码</h1>" +
                "<p style='text-align:center'>尊敬的用户你好，请复制下列验证码完成账号登录(1小时内有效时间)</p>" +
                "<p style='text-align:center'>%s</p>", text);
        return new Email(to, "验证码", text);
    }

    public void sendSimpleMail(Email email) {
        System.out.println(username);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(email.getTo());
        message.setSubject(email.getSubject());
        message.setText(email.getText());
        System.out.println(message);
        mailSender.send(message);
    }

    public void sendHtmlMail(Email email) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(username);
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText(email.getText(), true);
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
    }

    public void sendAttachmentsMail(Email email) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(username);
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText(email.getText());
            FileSystemResource file = new FileSystemResource(new File("spring.log"));
            helper.addAttachment("附件-1.jpg", file);
            helper.addAttachment("附件-2.jpg", file);
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
    }

    public void sendInlineMail(Email email) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(username);
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText("<html><body><img src=\"cid:weixin\" ></body></html>", true);
            FileSystemResource file = new FileSystemResource(new File("weixin.jpg"));
            helper.addInline("weixin", file);
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
    }

    class Email {
        private String to;
        private String subject;
        private String text;
        private FileSystemResource[] files;

        public Email() {
        }

        public Email(String to, String subject, String text) {
            this.to = to;
            this.subject = String.format("[%s]", subject);
            this.text = text;
        }

        public Email(String to, String subject, String text, FileSystemResource[] files) {
            this.to = to;
            this.subject = String.format("[%s]", subject);
            this.text = text;
            this.files = files;
        }


        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public FileSystemResource[] getFiles() {
            return files;
        }

        public void setFiles(FileSystemResource[] files) {
            this.files = files;
        }
    }
}
