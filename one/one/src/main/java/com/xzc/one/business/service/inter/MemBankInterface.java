package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemBank;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员银行卡表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemBankInterface extends IService<MemBank> {

}
