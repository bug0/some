package com.xzc.one.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xzc.one.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author 熊智聪
 * @create 2019-10-22 16:28
 **/
@Mapper
@Repository
public interface UserMapper extends BaseMapper<User> {
    @Select("select user_name from sys_user where id = 1")
    String select();

    String selectUserByName(int id);
}
