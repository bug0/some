package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 广告点击数释放详情
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemAdReleasDetail extends Model<MemAdReleasDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 记录id
     */
    private Integer memAdReleasId;

    /**
     * 释放日期
     */
    private LocalDate releasDate;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 释放点数
     */
    private Integer adCount;

    /**
     * 释放金额
     */
    private BigDecimal balanceAmount;

    /**
     * 释放点数
     */
    private BigDecimal couponAmount;

    /**
     * 交易编号
     */
    private String tradeSn;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
