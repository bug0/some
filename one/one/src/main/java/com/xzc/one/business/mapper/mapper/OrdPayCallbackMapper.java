package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.OrdPayCallback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdPayCallbackMapper extends BaseMapper<OrdPayCallback> {

}
