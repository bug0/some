package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * vr设备商品表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProVrDevice extends Model<ProVrDevice> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 0申请成为合伙人商品，1普通商品
     */
    private Integer proType;

    /**
     * 厂商id
     */
    private Integer proProducerId;

    /**
     * 厂商名称
     */
    private String proProducerName;

    /**
     * 名称
     */
    private String name;

    /**
     * 上架状态 0下架，1上架
     */
    private Integer status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 设备型号名称
     */
    private String modelName;

    /**
     * 设备价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer storeNumber;

    /**
     * 列表图片
     */
    private String logo;

    /**
     * 详情宣传
     */
    private String logoList;

    /**
     * 图文详情
     */
    private String content;

    /**
     * 销量
     */
    private Integer salesVolume;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
