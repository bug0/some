package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员银行卡表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemBank extends Model<MemBank> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer memberId;

    /**
     * 0银行卡，1微信，2支付宝
     */
    private Integer type;

    /**
     * 持卡人
     */
    private String cardHolder;

    /**
     * 银行卡号（或者微信支付宝收款二维码）
     */
    private String cardNo;

    /**
     * 所属银行
     */
    private String ownBank;

    /**
     * 银行卡预留手机号
     */
    private String cardMobile;

    /**
     * 开户行
     */
    private String cardAddress;

    /**
     * 银行账户类型：0个人，1公司
     */
    private Integer cardType;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
