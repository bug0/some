package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.SysRole;
import com.xzc.one.business.mapper.mapper.SysRoleMapper;
import com.xzc.one.business.service.inter.SysRoleInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台角色 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleInterface {

}
