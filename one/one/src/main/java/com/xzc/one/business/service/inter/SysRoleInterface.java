package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台角色 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SysRoleInterface extends IService<SysRole> {

}
