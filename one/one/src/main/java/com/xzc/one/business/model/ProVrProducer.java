package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * vr设备供应商表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProVrProducer extends Model<ProVrProducer> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 厂商名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String connectMobile;

    /**
     * 联系人姓名
     */
    private String connectName;

    /**
     * 品牌图片
     */
    private String logo;

    /**
     * 0未上架，1已上架
     */
    private Integer status;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
