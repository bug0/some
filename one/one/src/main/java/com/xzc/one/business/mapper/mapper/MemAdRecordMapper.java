package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAdRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员广告点击记录表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdRecordMapper extends BaseMapper<MemAdRecord> {

}
