package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.Orders;
import com.xzc.one.business.mapper.mapper.OrdersMapper;
import com.xzc.one.business.service.inter.OrdersInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class OrdersService extends ServiceImpl<OrdersMapper, Orders> implements OrdersInterface {

}
