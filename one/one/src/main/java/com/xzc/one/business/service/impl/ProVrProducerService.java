package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.ProVrProducer;
import com.xzc.one.business.mapper.mapper.ProVrProducerMapper;
import com.xzc.one.business.service.inter.ProVrProducerInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * vr设备供应商表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class ProVrProducerService extends ServiceImpl<ProVrProducerMapper, ProVrProducer> implements ProVrProducerInterface {

}
