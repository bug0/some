package com.xzc.one.common.ce;

/**
 * MyClass
 *
 * @author xiongzhicong
 * @create 2019-11-06 13:15
 **/
public class MyClass {

    public static final String APP = "APPName";
    public static final String Token = "Token";
    public static final String Register = "Register";
    public static final String ChangePassword = "ChangePassword";
    public static final String ChangePhone = "ChangePhone";
    public static final String ChangeEmail = "ChangeEmail";
    public static final String Prefix_NO = "NO";
    public static final int Time_minute = 60;
    public static final int Time_hour = 60 * 60;
    public static final int Time_day = 60 * 60 * 24;
    public static final int Time_week = 60 * 60 * 24 * 7;
    public static final int Time_month = 60 * 60 * 24 * 7 * 30;
    //public static final int Time_year = 60 * 60 * 24 * 7 * 30 * 365;//数字溢出

}
