package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员收货地址表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAddressMapper extends BaseMapper<MemAddress> {

}
