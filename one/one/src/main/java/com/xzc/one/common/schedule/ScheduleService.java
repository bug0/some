package com.xzc.one.common.schedule;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * description: ScheduleService
 * create: XiongZhiCong 2019/11/19
 */
@Service
public class ScheduleService {
    public void print() {
        System.out.println("ScheduleService.print" + LocalDateTime.now());
    }
}
