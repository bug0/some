package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemAdReleasDetail;
import com.xzc.one.business.mapper.mapper.MemAdReleasDetailMapper;
import com.xzc.one.business.service.inter.MemAdReleasDetailInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告点击数释放详情 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemAdReleasDetailService extends ServiceImpl<MemAdReleasDetailMapper, MemAdReleasDetail> implements MemAdReleasDetailInterface {

}
