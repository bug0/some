package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.ProVideoPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * vr视频套餐表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVideoPriceMapper extends BaseMapper<ProVideoPrice> {

}
