package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemPartener;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 合伙人列表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemPartenerMapper extends BaseMapper<MemPartener> {

}
