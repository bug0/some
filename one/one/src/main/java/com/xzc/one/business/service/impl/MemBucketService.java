package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemBucket;
import com.xzc.one.business.mapper.mapper.MemBucketMapper;
import com.xzc.one.business.service.inter.MemBucketInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员购物车 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemBucketService extends ServiceImpl<MemBucketMapper, MemBucket> implements MemBucketInterface {

}
