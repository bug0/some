package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.OrdPayCallback;
import com.xzc.one.business.mapper.mapper.OrdPayCallbackMapper;
import com.xzc.one.business.service.inter.OrdPayCallbackInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class OrdPayCallbackService extends ServiceImpl<OrdPayCallbackMapper, OrdPayCallback> implements OrdPayCallbackInterface {

}
