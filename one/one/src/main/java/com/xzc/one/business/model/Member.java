package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Member extends Model<Member> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 上级id
     */
    private Integer parentId;

    /**
     * 场地合伙人id
     */
    private Integer shopOwnerId;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 微信小程序openId
     */
    private String wxProId;

    /**
     * 微信会话
     */
    private String wxSessionKey;

    /**
     * 邀请码
     */
    private String inviteCode;

    /**
     * 密码
     */
    private String password;

    /**
     * 交易密码
     */
    private String payPassword;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 头像
     */
    private String headPic;

    /**
     * 性别：0->未知；1->男；2->女
     */
    private Integer gender;

    /**
     * 钱包余额
     */
    private BigDecimal curBalance;

    /**
     * 总共金额（包括已使用的）
     */
    private BigDecimal totalBalance;

    /**
     * 消费点数余额
     */
    private BigDecimal curCoupon;

    /**
     * 消费点数总额（包括已使用的）
     */
    private BigDecimal totalCoupon;

    /**
     * 广告剩余点数
     */
    private Integer curAdPoint;

    /**
     * 总广告点数
     */
    private Integer totalAdPoint;

    /**
     * 提现门槛限制，普通会员默认100,
     */
    private BigDecimal cashLimit;

    /**
     * 设备个数
     */
    private Integer devCount;

    /**
     * 状态，0启用，1禁用
     */
    private Integer status;

    /**
     * 注册时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
