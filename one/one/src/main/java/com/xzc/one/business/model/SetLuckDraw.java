package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 抽奖设置
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SetLuckDraw extends Model<SetLuckDraw> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 0禁用，1启用
     */
    private Integer enable;

    /**
     * 抽奖主题
     */
    private String name;

    /**
     * 中奖概率
     */
    private BigDecimal percent;

    /**
     * 抽奖图片
     */
    private String drawPic;

    /**
     * 抽奖结果图片
     */
    private String drawResPic;

    /**
     * 已抽奖次数
     */
    private Integer drawCount;

    /**
     * 中奖数字
     */
    private String luckyNumber;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 0未中奖，1中奖
     */
    private Integer lucky;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
