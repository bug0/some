package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAdReleasDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 广告点击数释放详情 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdReleasDetailInterface extends IService<MemAdReleasDetail> {

}
