package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemberMapper extends BaseMapper<Member> {

}
