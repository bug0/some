package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Orders extends Model<Orders> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 会员手机号
     */
    private String memberMobile;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 总金额
     */
    private BigDecimal orderAmount;

    /**
     * 订单标题
     */
    private String orderTitle;

    /**
     * 订单内容
     */
    private String orderContent;

    /**
     * 应付金额（实际支付金额）
     */
    private BigDecimal payAmount;

    /**
     * 优惠券抵扣金额
     */
    private BigDecimal couponAmount;

    /**
     * 支付方式：0->未支付；1->微信（APP）；2->支付宝；3->钱包；4钱包+抵扣，5线下，6微信小程序
     */
    private Integer payWay;

    /**
     * 订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭； 
     */
    private Integer status;

    /**
     * 订单类型：VR套餐0，普通用户购买VR设备1，合伙人购买VR设备2
     */
    private Integer orderType;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人电话
     */
    private String receiverMobile;

    /**
     * 省份/直辖市
     */
    private String receiverProvince;

    /**
     * 省份/直辖市id
     */
    private Integer receiverProvinceId;

    /**
     * 城市
     */
    private String receiverCity;

    /**
     * 城市id
     */
    private String receiverCityId;

    /**
     * 区
     */
    private String receiverRegion;

    /**
     * 区域id
     */
    private String receiverRegionId;

    /**
     * 详细地址
     */
    private String receiverDetailAddress;

    /**
     * 订单备注
     */
    private String note;

    /**
     * 支付时间
     */
    private LocalDateTime paymentTime;

    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;

    /**
     * 物流公司(配送方式)
     */
    private String deliveryCompany;

    /**
     * 物流单号
     */
    private String deliverySn;

    /**
     * 确认收货时间
     */
    private LocalDateTime receiveTime;

    /**
     * 确认收货状态：0->未确认；1->已确认
     */
    private Integer confirmStatus;

    /**
     * 提交时间（下单时间）
     */
    private LocalDateTime createTime;

    /**
     * 合伙人订单支付凭证
     */
    private String paySign;

    /**
     * 合伙人订单审核0未审核，1已审核，2未通过
     */
    private Integer authorStatus;

    /**
     * 合伙人设备绑定状态：0未绑定，1已绑定
     */
    private Integer bindStatus;

    /**
     * 设备投放方式0个人，1公司
     */
    private Integer bindType;

    /**
     * 门店投放状态：0未投放，1已投放
     */
    private Integer shopStatus;

    /**
     * 原因
     */
    private String resion;

    /**
     * 审核账号
     */
    private String authorUser;

    /**
     * 审核时间
     */
    private LocalDateTime authorTime;

    /**
     * 完全绑定时间
     */
    private LocalDateTime bindCompleteTime;

    /**
     * 0未抽奖，1以抽奖
     */
    private Integer draw;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
