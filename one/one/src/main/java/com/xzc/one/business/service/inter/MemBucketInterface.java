package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemBucket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员购物车 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemBucketInterface extends IService<MemBucket> {

}
