package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
