package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包收支记录
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemBalanceRecord extends Model<MemBalanceRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 交易分类0收益，1提现，2转账，3后台出入账 4，消费使用，5广告释放收益
     */
    private Integer tradeCat;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 交易编号
     */
    private String tradeSn;

    /**
     * 对方账号
     */
    private Integer sideMemberId;

    /**
     * 交易类型：0收入，1支出
     */
    private Integer tradeType;

    /**
     * 交易金额或积分的收支大小
     */
    private BigDecimal tradeAmount;

    /**
     * 0钱包，2消费点数
     */
    private Integer amountType;

    /**
     * 交易标题
     */
    private String tradeTitle;

    /**
     * 交易内容
     */
    private String tradeContent;

    /**
     * 比率
     */
    private BigDecimal rate;

    /**
     * 备注
     */
    private String remark;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
