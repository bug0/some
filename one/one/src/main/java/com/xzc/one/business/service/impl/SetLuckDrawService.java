package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.SetLuckDraw;
import com.xzc.one.business.mapper.mapper.SetLuckDrawMapper;
import com.xzc.one.business.service.inter.SetLuckDrawInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抽奖设置 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class SetLuckDrawService extends ServiceImpl<SetLuckDrawMapper, SetLuckDraw> implements SetLuckDrawInterface {

}
