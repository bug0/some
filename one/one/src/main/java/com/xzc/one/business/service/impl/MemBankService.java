package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemBank;
import com.xzc.one.business.mapper.mapper.MemBankMapper;
import com.xzc.one.business.service.inter.MemBankInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员银行卡表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemBankService extends ServiceImpl<MemBankMapper, MemBank> implements MemBankInterface {

}
