package com.xzc.one.common.schedule;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@EnableScheduling
public class ScheduleTask {

    @Resource
    ScheduleService scheduleService;

    //每秒钟执行一次
    @Scheduled(cron = "0/1 * * * * ?")
    public void taskSecond() {
        scheduleService.print();
    }

    //每分钟执行一次
    @Scheduled(cron = "0 0/1 * * * ?")
    public void taskMinute() {
    }

    //每小时执行一次
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void taskHour() {
    }

    //每天零点执行一次
    @Scheduled(cron = "0 0 0 * * ?")
    public void taskDay() {
    }

    //每周一零点执行一次
    @Scheduled(cron = "0 0 0 ? * MON")
    public void taskWeek() {
    }

    //每月1日零点执行一次
    @Scheduled(cron = "0 0 0 1 * ?")
    public void taskMonth() {
    }

    //每年1月1日零点执行一次
    @Scheduled(cron = "0 0 0 1 1 ?")
    public void taskYear() {
    }

}
