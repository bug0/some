package com.xzc.one.business.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * vr设备供应商表 前端控制器
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Controller
@RequestMapping("/proVrProducer")
public class ProVrProducerController {

}

