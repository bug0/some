package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.ProVideoPrice;
import com.xzc.one.business.mapper.mapper.ProVideoPriceMapper;
import com.xzc.one.business.service.inter.ProVideoPriceInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * vr视频套餐表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class ProVideoPriceService extends ServiceImpl<ProVideoPriceMapper, ProVideoPrice> implements ProVideoPriceInterface {

}
