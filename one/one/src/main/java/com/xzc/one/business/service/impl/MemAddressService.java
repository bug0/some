package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemAddress;
import com.xzc.one.business.mapper.mapper.MemAddressMapper;
import com.xzc.one.business.service.inter.MemAddressInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收货地址表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemAddressService extends ServiceImpl<MemAddressMapper, MemAddress> implements MemAddressInterface {

}
