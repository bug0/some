package com.xzc.one.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xzc.one.business.mapper.mapper.SysUserMapper;
import com.xzc.one.business.model.SysUser;
import com.xzc.one.business.service.inter.SysUserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> implements SysUserInterface {

    @Autowired
    private SysUserMapper sysUserMapper;

    public SysUser sys() {
        return sysUserMapper.selectOne(new QueryWrapper<SysUser>().eq("id", "1"));
    }
}
