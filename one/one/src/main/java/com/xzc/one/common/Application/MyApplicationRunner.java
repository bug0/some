package com.xzc.one.common.Application;

import com.xzc.one.common.ce.MyClass;
import com.xzc.one.common.util.ToolUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.*;

/**
 * @author xiongzhicong
 * @create 2019-11-07 13:25
 **/
@Component
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Override
    public void run(ApplicationArguments args) {
        Map<RequestMappingInfo, HandlerMethod> handlerMethodMap = requestMappingHandlerMapping.getHandlerMethods();
        List<Map<String, String>> list = new LinkedList<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : handlerMethodMap.entrySet()) {
            Map<String, String> map = new HashMap<>();
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            PatternsRequestCondition p = info.getPatternsCondition();
            for (String url : p.getPatterns()) {
                map.put("url", url);
            }
            map.put("className", method.getMethod().getDeclaringClass().getName()); // 类名
            map.put("methodName", method.getMethod().getName()); // 方法名
            list.add(map);
        }
        //ToolUtil.PrintUtil.print(list);
    }
}
