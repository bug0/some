package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAdReleas;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 广告点数释放记录 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdReleasInterface extends IService<MemAdReleas> {

}
