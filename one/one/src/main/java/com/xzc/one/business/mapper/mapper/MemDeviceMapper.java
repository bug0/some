package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemDevice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 合伙人购买设备投放列表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemDeviceMapper extends BaseMapper<MemDevice> {

}
