package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAgent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员代理商信息 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAgentInterface extends IService<MemAgent> {

}
