package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.ProVrShop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * vr门店列表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrShopInterface extends IService<ProVrShop> {

}
