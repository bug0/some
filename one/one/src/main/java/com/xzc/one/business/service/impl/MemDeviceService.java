package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemDevice;
import com.xzc.one.business.mapper.mapper.MemDeviceMapper;
import com.xzc.one.business.service.inter.MemDeviceInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 合伙人购买设备投放列表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemDeviceService extends ServiceImpl<MemDeviceMapper, MemDevice> implements MemDeviceInterface {

}
