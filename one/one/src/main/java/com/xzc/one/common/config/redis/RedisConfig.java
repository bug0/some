package com.xzc.one.common.config.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Data
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisConfig {

    private static String host;
    private static int port;
    private static String password;
    private static int timeout = 3;//秒
    private static int poolMaxTotal = 10;
    private static int poolMaxIdle = 10;
    private static int poolMaxWait = 3;//秒

    private static JedisPool jedisPool = null;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 初始化redis连接池
     */
    private static void initPool() {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(poolMaxTotal);//最大连接数
            config.setMaxIdle(poolMaxIdle);//最大空闲连接数
            config.setMaxWaitMillis(poolMaxWait);//获取可用连接的最大等待时间
            //jedisPool = new JedisPool(config, host, port);
            jedisPool = new JedisPool(config, host, port, timeout * 1000, password, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取jedis实例
     *
     * @return jedis实例
     */
    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool == null) {
                initPool();
            }
            return jedisPool.getResource();
            // jedis.auth("123456");//密码
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
