package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemAdRecord;
import com.xzc.one.business.mapper.mapper.MemAdRecordMapper;
import com.xzc.one.business.service.inter.MemAdRecordInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员广告点击记录表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemAdRecordService extends ServiceImpl<MemAdRecordMapper, MemAdRecord> implements MemAdRecordInterface {

}
