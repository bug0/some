package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemBalanceRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包收支记录 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemBalanceRecordMapper extends BaseMapper<MemBalanceRecord> {

}
