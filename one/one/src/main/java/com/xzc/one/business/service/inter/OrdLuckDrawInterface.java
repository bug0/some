package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.OrdLuckDraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单抽奖记录 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdLuckDrawInterface extends IService<OrdLuckDraw> {

}
