package com.xzc.one.common.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xiongzhicong
 * @create 2019-11-07 11:45
 **/
@Slf4j
@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static final String START_TIME = "requestStartTime";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            RequestMapping requestMapping = ((HandlerMethod) handler).getMethodAnnotation(RequestMapping.class);
            final String[] value = requestMapping.value();
            if (requestMapping != null) {
                System.out.println();
            } else {
                System.out.println("此Action方法上面有没注解！");
                return true;
            }
        }
        return super.preHandle(request, response, handler);
    }
//
//    private <T extends Annotation> T findAnnotation(HandlerMethod handler, Class<T> annotationType) {
//        T annotation = handler.getBeanType().getAnnotation(annotationType);
//        if (annotation != null) return annotation;
//        return handler.getMethodAnnotation(annotationType);
//    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}