package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员收货地址表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemAddress extends Model<MemAddress> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人电话
     */
    private String receiverMobile;

    /**
     * 省份/直辖市
     */
    private String receiverProvince;

    /**
     * 省份/直辖市id
     */
    private Integer receiverProvinceId;

    /**
     * 城市
     */
    private String receiverCity;

    /**
     * 城市id
     */
    private String receiverCityId;

    /**
     * 区
     */
    private String receiverRegion;

    /**
     * 区域id
     */
    private String receiverRegionId;

    /**
     * 详细地址
     */
    private String receiverDetailAddress;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
