package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员收货地址表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAddressInterface extends IService<MemAddress> {

}
