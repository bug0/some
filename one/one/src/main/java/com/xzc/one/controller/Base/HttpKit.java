package com.xzc.one.controller.Base;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

/**
 * http请求
 *
 * @author xzcong
 * @create 2019-10-20 14:32
 **/
public class HttpKit {
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected HttpServletResponse response;
    @Autowired
    protected HttpSession session;

    protected String getIP() {
        return request.getRemoteHost();
    }

    protected String getURI() {
        return request.getRequestURI();//获取请求资源
    }

    protected String getURL() {
        return request.getRequestURL().toString();//获取get请求参数
    }

    protected String getQueryString() {
        return request.getQueryString();//获取get请求参数
    }

    protected String getContextPath() {
        return request.getContextPath();//获取当前web应用名称
    }

    protected String getSessionID() {
        return session.getId();//获取当前sessionID
    }

    protected void printSession() {
        System.out.println(session.getId());
        Enumeration<?> enumeration = session.getAttributeNames();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement().toString();
            Object value = session.getAttribute(name);
            System.out.println(name + "<<<<<<=========================>>>>>>>" + value);
        }
    }

    protected void setSession(String name, Object value) {
        session.setAttribute(name, value);
    }

    protected Object getSession(String name) {
        return session.getAttribute(name);
    }

    protected void printAttribute() {
        Enumeration<?> enumeration = request.getAttributeNames();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement().toString();
            Object value = request.getAttribute(name);
            System.out.println(name + "<<<<<<=========================>>>>>>>" + value);
        }
    }

    protected void setAttribute(String name, Object value) {
        request.setAttribute(name, value);
    }

    protected Object getAttribute(String name) {
        return request.getAttribute(name);
    }

    protected void printParameter() {
        Enumeration<?> enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement().toString();
            String value = request.getParameter(name);
            printKeyValue(name, value);
        }
    }

    protected String getParameter(String name) {
        return request.getParameter(name);
    }

    protected void printHeader() {
        Enumeration<?> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement().toString();
            String value = request.getHeader(name);
            printKeyValue(name, value);
        }
    }

    protected String getHeader(String name) {
        return request.getHeader(name);
    }

    /**
     * 头参数名称中 Referer 代表从哪个页面发送过来的信息，用refer可以防盗链（爬虫），
     * 这样只能是自己服务器的域名网址才能访问，否则不能访问，
     * refer只有在 a标签、input标签或是submit提交的表单(POST或GET)、
     * JavaScript提交的表单（POST或GET）才能获取，
     * 其他的方式很可能无效（例如直接在地址栏中输入地址访问）
     *
     * @return
     */
    protected String get() {
        return getHeader("Referer");
    }

    protected void printCookie() {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            String value = cookie.getValue();
            printKeyValue(name, value);
        }
    }

    protected void setCookie(String name, String value, int time) {
        Cookie cookie = new Cookie(name, value);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        cookie.setMaxAge(time);//缓存时间
        response.addCookie(cookie);
    }

    protected String getCookie(String name) {
        if (request.getCookies() == null) return null;
        List<Cookie> cookies = Arrays.stream(request.getCookies()).filter(r -> r.getName().equals(name)).collect(Collectors.toList());
        return cookies.size() > 0 ? cookies.get(0).getValue() : null;
    }

    private void printKeyValue(String name, String value) {
        System.out.println(name + "<<<<<<=========================>>>>>>>" + value);
    }
}
