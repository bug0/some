package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemDevice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 合伙人购买设备投放列表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemDeviceInterface extends IService<MemDevice> {

}
