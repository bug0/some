package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SetItems;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设置项 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetItemsInterface extends IService<SetItems> {

}
