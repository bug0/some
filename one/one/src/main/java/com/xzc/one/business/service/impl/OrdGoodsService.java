package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.OrdGoods;
import com.xzc.one.business.mapper.mapper.OrdGoodsMapper;
import com.xzc.one.business.service.inter.OrdGoodsInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单商品表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class OrdGoodsService extends ServiceImpl<OrdGoodsMapper, OrdGoods> implements OrdGoodsInterface {

}
