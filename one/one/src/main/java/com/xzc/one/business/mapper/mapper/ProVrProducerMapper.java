package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.ProVrProducer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * vr设备供应商表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrProducerMapper extends BaseMapper<ProVrProducer> {

}
