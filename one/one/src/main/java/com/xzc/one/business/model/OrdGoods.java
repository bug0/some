package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单商品表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OrdGoods extends Model<OrdGoods> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品logo
     */
    private String goodsLogo;

    /**
     * 公司分类名称
     */
    private String goodsProducer;

    /**
     * 总价
     */
    private BigDecimal totalPrice;

    /**
     * 数目
     */
    private Integer goodsCount;

    /**
     * 投放设备记录id
     */
    private Integer memDeviceId;

    /**
     * 设备编号
     */
    private String memDeviceMac;

    /**
     * 设备合伙人id
     */
    private Integer memPartenerId;

    /**
     * 扫码门店id
     */
    private Integer proShopId;

    /**
     * 时长（分钟）
     */
    private Integer duration;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
