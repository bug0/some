package com.xzc.one.common.msc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xzc.one.business.mapper.mapper.SysUserMapper;
import com.xzc.one.business.model.SysUser;
import com.xzc.one.common.inout.Pager;
import com.xzc.one.common.inout.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author xiongzhicong
 * @create 2019-11-06 19:05
 **/
@Service
public class UserService {
    @Autowired
    SysUserMapper sysUserMapper;

    Result login(Map<String, Object> param) {
        Pager pager = new Pager(param);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        IPage iPage = sysUserMapper.selectPage(pager, queryWrapper);
        return Result.success(sysUserMapper.selectPage(pager, queryWrapper));
    }
}
