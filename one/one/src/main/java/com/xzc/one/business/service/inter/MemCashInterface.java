package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemCash;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 提现记录表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemCashInterface extends IService<MemCash> {

}
