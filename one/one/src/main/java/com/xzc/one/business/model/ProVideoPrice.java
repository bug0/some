package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * vr视频套餐表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProVideoPrice extends Model<ProVideoPrice> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 套餐名称
     */
    private String name;

    /**
     * 上架状态 0下架，1上架
     */
    private Integer status;

    /**
     * 套餐价格
     */
    private BigDecimal price;

    /**
     * 时长（分钟）
     */
    private Integer duration;

    /**
     * 销量
     */
    private Integer salesVolume;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
