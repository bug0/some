package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.ComBankData;
import com.xzc.one.business.mapper.mapper.ComBankDataMapper;
import com.xzc.one.business.service.inter.ComBankDataInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class ComBankDataService extends ServiceImpl<ComBankDataMapper, ComBankData> implements ComBankDataInterface {

}
