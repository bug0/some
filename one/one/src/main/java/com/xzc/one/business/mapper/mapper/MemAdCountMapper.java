package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAdCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 广告计数 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdCountMapper extends BaseMapper<MemAdCount> {

}
