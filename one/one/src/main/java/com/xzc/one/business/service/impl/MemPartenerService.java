package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemPartener;
import com.xzc.one.business.mapper.mapper.MemPartenerMapper;
import com.xzc.one.business.service.inter.MemPartenerInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 合伙人列表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemPartenerService extends ServiceImpl<MemPartenerMapper, MemPartener> implements MemPartenerInterface {

}
