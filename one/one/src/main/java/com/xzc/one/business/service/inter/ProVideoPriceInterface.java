package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.ProVideoPrice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * vr视频套餐表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVideoPriceInterface extends IService<ProVideoPrice> {

}
