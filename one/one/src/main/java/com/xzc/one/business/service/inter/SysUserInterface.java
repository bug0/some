package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SysUserInterface extends IService<SysUser> {

}
