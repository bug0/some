package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemPartener;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 合伙人列表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemPartenerInterface extends IService<MemPartener> {

}
