package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.OrdIncome;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单收益表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdIncomeMapper extends BaseMapper<OrdIncome> {

}
