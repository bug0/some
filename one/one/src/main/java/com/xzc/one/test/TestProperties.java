package com.xzc.one.test;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Component
@ConfigurationProperties(prefix = "var")
@PropertySource(value = "config.properties")
public class TestProperties {

    public String pfilename;

    @PostConstruct
    public void printFilename() {
        System.out.println(pfilename);
    }

}
