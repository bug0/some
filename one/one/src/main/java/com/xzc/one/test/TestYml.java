package com.xzc.one.test;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * description: TestYml
 * create: XiongZhiCong 2019/11/12 18:25
 */
@Data
@Component
@ConfigurationProperties(value = "var")
public class TestYml {

    public String filename;

    @PostConstruct
    public void printFilename() {
        System.out.println(filename);
    }
}
