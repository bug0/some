package com.xzc.one.common.ce;

/**
 * 枚举
 *
 * @author xzcong
 * @create 2019-10-20 13:05
 **/
public class MyEnum {
    // 性别
    public enum GenderStatus {

        Male("男", 1),
        Female("女", 0);
        private String name;
        private Integer value;

        GenderStatus(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public Integer getValue() {
            return value;
        }

        public static String getNameByValue(Integer value) {
            for (GenderStatus g : GenderStatus.values()) {
                if (g.getValue().equals(value)) {
                    return g.getName();
                }
            }
            return "未知";
        }
    }

    // 禁用启用状态
    public enum EnabileStatus {

        Enable("启用", 0),
        Disable("禁用", -1);
        private String name;
        private Integer value;

        EnabileStatus(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public Integer getValue() {
            return value;
        }

        public static String getNameByValue(Integer value) {
            for (EnabileStatus e : EnabileStatus.values()) {
                if (e.getValue().equals(value)) {
                    return e.getName();
                }
            }
            return "未知";
        }
    }
}
