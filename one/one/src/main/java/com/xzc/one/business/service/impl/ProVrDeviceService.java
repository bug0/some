package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.ProVrDevice;
import com.xzc.one.business.mapper.mapper.ProVrDeviceMapper;
import com.xzc.one.business.service.inter.ProVrDeviceInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * vr设备商品表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class ProVrDeviceService extends ServiceImpl<ProVrDeviceMapper, ProVrDevice> implements ProVrDeviceInterface {

}
