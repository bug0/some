package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAgent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员代理商信息 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAgentMapper extends BaseMapper<MemAgent> {

}
