package com.xzc.one.common.config.redis;

import com.alibaba.fastjson.JSON;
import redis.clients.jedis.Jedis;

public class RedisUtil {

    public static final int Time_minute = 60;
    public static final int Time_hour = 60 * 60;
    public static final int Time_day = 60 * 60 * 24;
    public static final int Time_week = 60 * 60 * 24 * 7;
    public static final int Time_month = 60 * 60 * 24 * 7 * 30;
    //public static final int Time_year = 60 * 60 * 24 * 7 * 30 * 365;//数字溢出
    private static String className = "";
    public static final String Field_ID = "id";
    public static final String Field_NAME = "name";
    public static final String Code_image = "code";
    public static final String Type_ArrayList = "ArrayList";


    private static String getPrefix(String className, String field, String key) {
        return className + ":" + field + ":" + key;
    }

    // 获取单个对象
    public static <T> T get(Class<T> clazz, String field, String key) {
        Jedis jedis = RedisConfig.getJedis();
        className = clazz.getSimpleName();
        String realKey = getPrefix(className, field, key);
        String str = jedis.get(realKey);
        T t = JSON.parseObject(str, clazz);
        jedis.close();
        return t;
    }

    // 设置对象
    public static <T> boolean set(T obj, String field, String key) {
        return set(obj, field, key, 0);
    }

    public static <T> boolean set(T obj, String field, String key, int expire) {
        Jedis jedis = RedisConfig.getJedis();
        className = obj.getClass().getSimpleName();
        String str = JSON.toJSONString(obj);
        if (str == null || str.length() <= 0) {
            jedis.close();
            return false;
        }
        String realKey = getPrefix(className, field, key);
        if (expire <= 0) {
            jedis.set(realKey, str);
        } else {
            jedis.setex(realKey, expire, str);
        }
        jedis.close();
        return true;
    }

    // 判断key是否存在
    public static <T> boolean exists(Class<T> clazz, String field, String key) {
        Jedis jedis = RedisConfig.getJedis();
        className = clazz.getSimpleName();
        String realKey = getPrefix(className, field, key);
        boolean result = jedis.exists(realKey);
        jedis.close();
        return result;
    }

    // 增加值
    public static <T> Long incr(Class<T> clazz, String field, String key) {
        Jedis jedis = RedisConfig.getJedis();
        className = clazz.getSimpleName();
        String realKey = getPrefix(className, field, key);
        Long result = jedis.incr(realKey);
        return result;
    }

    // 减少值
    public static <T> Long decr(Class<T> clazz, String field, String key) {
        Jedis jedis = RedisConfig.getJedis();
        className = clazz.getSimpleName();
        String realKey = getPrefix(className, field, key);
        Long result = jedis.decr(realKey);
        jedis.close();
        return result;
    }

    public static <T> boolean set(T obj, String key) {
        return set(obj, key, Time_week);
    }

    public static <T> boolean set(T obj, String key, int expire) {
        Jedis jedis = RedisConfig.getJedis();
        String str = JSON.toJSONString(obj);
        if (str == null || str.length() <= 0) {
            jedis.close();
            return false;
        }
        if (expire <= 0) {
            jedis.set(key, str);
        } else {
            jedis.setex(key, expire, str);
        }
        jedis.close();
        return true;
    }

    public static <T> T get(Class<T> clazz, String key) {
        Jedis jedis = RedisConfig.getJedis();
        String str = jedis.get(key);
        T t = JSON.parseObject(str, clazz);
        jedis.close();
        return t;
    }

    public static <T> boolean exists(Class<T> clazz, String key) {
        Jedis jedis = RedisConfig.getJedis();
        boolean result = jedis.exists(key);
        jedis.close();
        return result;
    }
}
