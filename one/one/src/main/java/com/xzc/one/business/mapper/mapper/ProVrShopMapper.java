package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.ProVrShop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * vr门店列表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrShopMapper extends BaseMapper<ProVrShop> {

}
