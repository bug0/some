package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.ProVrProducer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * vr设备供应商表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrProducerInterface extends IService<ProVrProducer> {

}
