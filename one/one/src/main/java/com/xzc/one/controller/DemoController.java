package com.xzc.one.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xzc.one.business.mapper.mapper.SysUserMapper;
import com.xzc.one.business.model.SysUser;
import com.xzc.one.common.ce.MyEnum;
import com.xzc.one.common.inout.Pager;
import com.xzc.one.common.inout.Request;
import com.xzc.one.common.pay.alipay.AliPay;
import com.xzc.one.common.util.FileUtil;
import com.xzc.one.common.util.ObjectMapUtil;
import com.xzc.one.common.util.SmsUtil;
import com.xzc.one.common.util.ToolUtil;
import com.xzc.one.controller.Base.UserKit;
import com.xzc.one.mapper.UserMapper;
import com.xzc.one.model.User;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author xiongzhicong
 * @create 2019-10-27 16:33
 **/
@RestController
@RequestMapping("/demo")
public class DemoController extends UserKit {

    @Autowired
    UserMapper userMapper;
    @Autowired
    SysUserMapper sysUserMapper;
    @Autowired
    AliPay aliPay;

    @RequestMapping("/enumDemo")
    public String enumDemo(@RequestBody Request<Map<String, Object>> param) {
        param.print();
        List<User> user = new LinkedList<>();
        user.add(new User("aaa", "111", 1));
        user.add(new User("bbb", "222", 0));
        for (User u : user) {
            System.out.println(MyEnum.GenderStatus.getNameByValue(Integer.valueOf(u.getGenderStatus())));
        }
        MyEnum.GenderStatus g = MyEnum.GenderStatus.Male;
        //r.put("statusStr", MyEnum.GenderStatus.getNameByValue(Integer.valueOf(r.get("status"))));
        System.out.println(param.getData().get("username"));
        return param.toString();
    }

    @RequestMapping("/printParam")
    public String printParam(@RequestBody Request<Map<String, Object>> param) {
        System.out.println("DemoController.printParam");
        System.out.println(param.toString());
        ToolUtil.PrintUtil.print(param.getData());
        ToolUtil.PrintUtil.print(param.getData().getClass().toString(), param.getData().get("aa").getClass().toString());
        return param.toString();
    }

    @RequestMapping("/bbb")
    public String bbb(String username, String password) {
        System.out.println("Demo.bbb");
        System.out.println(username);
        return username;
    }

    @RequestMapping("/sql")
    public String sql() {
        System.out.println("Demo.sql");
        System.out.println(userMapper.select());
        System.out.println(userMapper.selectUserByName(1));
        return userMapper.select();
    }


    @RequestMapping("/getDx")
    public String getDx() {
        System.out.println("DemoController.a");
        SmsUtil.getDx("15979177296", SmsUtil.DX_DEFAULT, "123");
        return "/getDx";
    }

    @RequestMapping("/pageDemo")
    public Object pageDemo() {
        System.out.println("DemoController.pageDemo");
        Pager pager = new Pager();
        QueryWrapper queryWrapper = new QueryWrapper<>();
        IPage<SysUser> userIPage = sysUserMapper.selectPage(pager, queryWrapper);
        System.out.println(userIPage);
        return userIPage;
    }

    @RequestMapping("/alipay")
    public Object alipay() {
        System.out.println("DemoController.alipay");
        aliPay.creatPcOrder("alipay", "001", 1);
        return "userIPage";
    }

    @RequestMapping("/returnUrl")
    public Object returnUrl() {
        System.out.println("DemoController.returnUrl");
        return "returnUrl";
    }

    @RequestMapping("/notifyUrl")
    public Object notifyUrl() {
        System.out.println("DemoController.notifyUrl");
        return "notifyUrl";
    }

    /**
     * map导出
     */
    @RequestMapping("/exportm")
    public void exportm() {
        //标题
        List<ExcelExportEntity> entityList = new ArrayList<>();
        //内容
        List<Map<String, Object>> dataResult = new ArrayList<>();
        entityList.add(new ExcelExportEntity("表头1", "table1", 15));
        entityList.add(new ExcelExportEntity("表头2", "table2", 25));
        entityList.add(new ExcelExportEntity("表头3", "table3", 35));
        for (int i = 0; i < 10; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("table1", "苹果" + i);
            map.put("table2", "香蕉" + i);
            map.put("table3", "鸭梨" + i);
            dataResult.add(map);
        }
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("测试", "测试"), entityList,
                dataResult);
        FileUtil.downLoadExcel("shuiguo.xls", response, workbook);

    }

    public Workbook export(List<String> titleList, List<Map<String, Object>> dataList, String title, String sheetName) {
        if (titleList.size() != dataList.get(0).size()) return null;
        //标题
        List<ExcelExportEntity> entityList = new ArrayList<>();
        for (int i = 0; i < titleList.size(); i++) {
            entityList.add(new ExcelExportEntity(titleList.get(i), titleList.get(i), 10));
        }
        //内容
        List<Map<String, Object>> dataResult = new ArrayList<>();
        dataList.forEach(p -> {
            Map<String, Object> map = new HashMap<>();
            titleList.forEach(q -> {
                map.put(q, p.get(q));
            });
            dataResult.add(map);
        });
        return ExcelExportUtil.exportExcel(new ExportParams(title, sheetName), entityList, dataResult);
    }

    public Workbook export(List dataList, String title, String sheetName) {
        //标题
        if (ToolUtil.IsUtil.isNull(dataList)) return null;
        List<String> key = new ArrayList<>(ObjectMapUtil.obj2Map(dataList.get(0)).keySet());
        //内容
        List<Map<String, Object>> dataResult = new LinkedList<>();
        dataList.forEach(r -> dataResult.add(ObjectMapUtil.obj2Map(r)));
        return export(key, dataResult, title, sheetName);
    }

    @RequestMapping("/aaa")
    public void aaa() {
        List<Map<String, Object>> dataResult = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("table1", "苹果" + i);
            map.put("table2", "香蕉" + i);
            map.put("table3", "鸭梨" + i);
            dataResult.add(map);
        }
        List<String> strings = Arrays.asList("苹果", "香蕉", "鸭梨");
        FileUtil.downLoadExcel("sss.xls", response, export(strings, dataResult, "aa", "aaaa"));
    }

    @RequestMapping("/export")
    public void export() {
        //模拟从数据库获取需要导出的数据
        List<User> personList = new ArrayList<>();
        User person1 = new User("路飞", "1", 1);
        User person2 = new User("娜美", "2", 1);
        User person3 = new User("索隆", "1", 1);
        User person4 = new User("小狸猫", "1", 1);
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        //导出操作
        // FileUtil.exportExcel(personList, "花名册", "草帽一伙", User.class, "海贼王.xls", response);
        FileUtil.downLoadExcel("sss.xls", response, export(personList, "aa", "aaaa"));
    }
}
