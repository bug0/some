package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemCash;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 提现记录表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemCashMapper extends BaseMapper<MemCash> {

}
