package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.SetLuckDraw;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 抽奖设置 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetLuckDrawMapper extends BaseMapper<SetLuckDraw> {

}
