package com.xzc.one.controller.Base;

import com.xzc.one.common.ce.MyClass;
import com.xzc.one.model.User;

/**
 * 关于用户网络请求
 *
 * @author xzcong
 * @create 2019-10-20 16:11
 **/
public class UserKit extends HttpKit {

    protected String getToken() {
        return getSessionID();
    }

    protected User getCurrentUser() {
        return (User) super.getSession(MyClass.Token);
    }

}
