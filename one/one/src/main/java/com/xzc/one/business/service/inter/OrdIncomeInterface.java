package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.OrdIncome;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单收益表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdIncomeInterface extends IService<OrdIncome> {

}
