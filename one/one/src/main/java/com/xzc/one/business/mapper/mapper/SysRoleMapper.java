package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台角色 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
