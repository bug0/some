package com.xzc.one.common.inout;

/**
 * 返回值
 *
 * @author xzcong
 * @create 2019-10-20 13:29
 **/
public class Result {

    private int code;
    private String msg;
    private Object data;

    //成功时候的调用
    public static Result success() {
        return new Result("成功");
    }

    public static Result success(String msg) {
        return new Result(msg);
    }

    public static Result success(Object data) {
        return new Result(data);
    }

    private Result(Object data) {
        this.code = CodeMsg.SUCCESS.getCode();
        this.msg = CodeMsg.SUCCESS.getMsg();
        this.data = data;
    }

    // 失败时候的调用
    public static Result error() {
        return new Result();
    }

    public static Result error(String msg) {
        return new Result(msg);
    }

    public static Result error(CodeMsg cm) {
        return new Result(cm);
    }

    private Result(CodeMsg cm) {
        if (cm == null) {
            this.code = CodeMsg.ERROR.getCode();
            this.msg = CodeMsg.ERROR.getMsg();
        }
        this.code = cm.getCode();
        this.msg = cm.getMsg();
    }

    private Result(String msg) {
        this.code = CodeMsg.ERROR.getCode();
        this.msg = CodeMsg.ERROR.getMsg();
        this.data = msg;
    }

    private Result() {
        this.code = CodeMsg.ERROR.getCode();
        this.msg = CodeMsg.ERROR.getMsg();
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Object getData() {
        return data;
    }
}
