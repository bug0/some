package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.OrdPayCallback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdPayCallbackInterface extends IService<OrdPayCallback> {

}
