package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.ProVrDevice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * vr设备商品表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrDeviceMapper extends BaseMapper<ProVrDevice> {

}
