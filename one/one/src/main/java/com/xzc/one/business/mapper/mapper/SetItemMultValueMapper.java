package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.SetItemMultValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设置值 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetItemMultValueMapper extends BaseMapper<SetItemMultValue> {

}
