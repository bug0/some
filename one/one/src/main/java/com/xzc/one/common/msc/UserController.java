package com.xzc.one.common.msc;

import com.xzc.one.common.inout.Request;
import com.xzc.one.common.inout.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author xiongzhicong
 * @create 2019-11-06 19:04
 **/
@RestController
public class UserController {

    @Autowired
    UserService userService;

    public Result login(@RequestBody Request<Map<String, Object>> param) {
        return userService.login(param.getData());
    }

}
