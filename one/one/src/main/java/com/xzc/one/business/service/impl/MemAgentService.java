package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemAgent;
import com.xzc.one.business.mapper.mapper.MemAgentMapper;
import com.xzc.one.business.service.inter.MemAgentInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员代理商信息 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemAgentService extends ServiceImpl<MemAgentMapper, MemAgent> implements MemAgentInterface {

}
