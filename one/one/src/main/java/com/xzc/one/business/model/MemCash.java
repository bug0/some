package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 提现记录表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemCash extends Model<MemCash> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 提现编号
     */
    private String cashSn;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 会员手机号
     */
    private String memberMobile;

    /**
     * 提现申请金额
     */
    private BigDecimal amount;

    /**
     * 实际打款金额
     */
    private BigDecimal remintAmount;

    /**
     * 手续费
     */
    private BigDecimal fee;

    /**
     * 手续费比例
     */
    private BigDecimal rate;

    /**
     * 提现方式：0银行卡，1微信，2支付宝
     */
    private Integer cashWay;

    /**
     * 持卡人
     */
    private String cardHolder;

    /**
     * 银行卡号
     */
    private String cardNo;

    /**
     * 所属银行
     */
    private String ownBank;

    /**
     * 银行卡预留手机号
     */
    private String cardMobile;

    /**
     * 银行账户类型：0个人，1公司
     */
    private Integer cardType;

    /**
     * 开户行
     */
    private String cardAddress;

    /**
     * 0未打款，1已打款
     */
    private Integer status;

    /**
     * 后台操作用户id
     */
    private Integer sysUserId;

    /**
     * 后台操作用户名称
     */
    private String sysUserName;

    /**
     * 打款时间
     */
    private LocalDateTime remitTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 申请时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
