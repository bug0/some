package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
