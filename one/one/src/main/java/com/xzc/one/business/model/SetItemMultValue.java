package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 设置值
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SetItemMultValue extends Model<SetItemMultValue> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer setItemsId;

    /**
     * 唯一编码
     */
    private String uniqueKey;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 设置子项名称
     */
    private String name;

    /**
     * 0长度比较小存在value1、value2中，1长度很长存在big_value内
     */
    private Integer valueType;

    /**
     * 设置值1
     */
    private String value1;

    /**
     * 设置值1
     */
    private String value2;

    /**
     * 超大值设置
     */
    private String bigValue;

    /**
     * 值说明
     */
    private String remark;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
