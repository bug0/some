package com.xzc.one.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * description: TestYml
 * create: XiongZhiCong 2019/11/12 18:25
 */
@Component
public class TestDb {
    @Value("${spring.datasource.name}")
    public String name;

    @PostConstruct
    public void printFilename() {
        System.out.println(name);
    }
}
