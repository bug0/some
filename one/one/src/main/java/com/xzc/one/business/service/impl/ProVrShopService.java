package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.ProVrShop;
import com.xzc.one.business.mapper.mapper.ProVrShopMapper;
import com.xzc.one.business.service.inter.ProVrShopInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * vr门店列表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class ProVrShopService extends ServiceImpl<ProVrShopMapper, ProVrShop> implements ProVrShopInterface {

}
