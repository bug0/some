package com.xzc.one.business.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 广告点击数释放详情 前端控制器
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Controller
@RequestMapping("/memAdReleasDetail")
public class MemAdReleasDetailController {

}

