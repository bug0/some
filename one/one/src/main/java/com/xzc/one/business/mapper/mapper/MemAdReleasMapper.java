package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAdReleas;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 广告点数释放记录 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdReleasMapper extends BaseMapper<MemAdReleas> {

}
