package com.xzc.one.common.msc;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.xzc.one.common.inout.Request;
import com.xzc.one.controller.Base.UserKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * commonController
 *
 * @author xiongzhicong
 * @create 2019-11-01 10:52
 **/
@RestController
@RequestMapping("/common")
public class CommonController extends UserKit {

    @Autowired
    CommonService commonService;

    @RequestMapping("/codeImage")
    public void codeImage() {
        commonService.codeImage();
    }

    @RequestMapping("/checkCodeImage")
    public void checkCodeImage(@RequestBody Request<Map<String, Object>> param) {
        commonService.checkCodeImage((String) param.getData().get("code"));
    }

    @RequestMapping("/QrImage")
    public void QrImage() {
        commonService.QrImage();
    }

    @RequestMapping("/url/{shortUrl}")
    public void shortUrl(@PathVariable(required = false) String shortUrl) {
        commonService.shortUrl(shortUrl);
    }

    @RequestMapping(value = "/upLoadFile")
    public void upLoadFile(@RequestBody @RequestParam("file") MultipartFile file) {
        System.out.println(commonService.upLoadFile(file));
    }

    @RequestMapping(value = "/redis")
    public void redis() {
        commonService.redis();
    }

    @RequestMapping("/email")
    public void email() {
        commonService.email();
    }

    @RequestMapping(value = "/loginRedis")
    public void loginRedis() {
        commonService.loginRedis();
    }

    @RequestMapping(value = "/get")
    public String get() {
        return "get";
    }

    @RequestMapping(value = "/post")
    public String post(@RequestParam("name") String name) {
        System.out.println(name);
        return "post";
    }

    @RequestMapping(value = "/jsonPost")
    public String jsonPost(@RequestBody Map<String, Object> param) {
        System.out.println(param.get("name"));
        return "jsonPost";
    }

    public static void main(String[] args) {
        // 最简单的HTTP请求，可以自动通过header等信息判断编码，不区分HTTP和HTTPS
        String result1 = HttpUtil.get("http://localhost:9100/get");
        System.out.println(result1);

        // 当无法识别页面编码的时候，可以自定义请求页面的编码
        String result2 = HttpUtil.get("http://localhost:9100/get", CharsetUtil.CHARSET_UTF_8);
        System.out.println(result2);

        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", "tyron");
        String result = HttpUtil.post("http://localhost:9100/post", paramMap);
        System.out.println(result);

        Map<String, Object> map = new HashMap<>();
        map.put("name", "tyron");
        map.put("age", 18);
        System.out.println(HttpUtil.post("http://localhost:9100/jsonPost", JSONUtil.parseFromMap(map).toString()));
    }
}
