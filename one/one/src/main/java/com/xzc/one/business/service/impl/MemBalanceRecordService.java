package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemBalanceRecord;
import com.xzc.one.business.mapper.mapper.MemBalanceRecordMapper;
import com.xzc.one.business.service.inter.MemBalanceRecordInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包收支记录 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemBalanceRecordService extends ServiceImpl<MemBalanceRecordMapper, MemBalanceRecord> implements MemBalanceRecordInterface {

}
