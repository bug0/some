package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemBucket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员购物车 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemBucketMapper extends BaseMapper<MemBucket> {

}
