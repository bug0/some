package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.ProVrDevice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * vr设备商品表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface ProVrDeviceInterface extends IService<ProVrDevice> {

}
