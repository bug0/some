package com.xzc.one.common.msc;

import com.xzc.one.common.config.redis.RedisUtil;
import com.xzc.one.common.util.EmailUtil;
import com.xzc.one.common.util.ToolUtil;
import com.xzc.one.controller.Base.HttpKit;
import com.xzc.one.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * CommonService
 *
 * @author xiongzhicong
 * @create 2019-11-02 13:51
 **/
@Service
public class CommonService extends HttpKit {

    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Autowired
    EmailUtil emailUtil;

    public void codeImage() {
        String code = ToolUtil.CodeImgUtil.getCode(4);
        setSession("codeImage", code);
        System.out.println((String) getSession("codeImage"));
        response.setContentType("image/jpeg");
        try {
            ImageIO.write(ToolUtil.CodeImgUtil.codeImg(code), "jpg", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkCodeImage(String code) {
        if (code == null || !code.equalsIgnoreCase((String) getSession("codeImg"))) {
            return false;
        }
        return true;
    }

    public void QrImage() {
        response.setContentType("image/jpeg");
        try {
            ToolUtil.QrCodeUtil.getQrImg("臭弟弟，你想看什么？", 300, 300, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shortUrl(String shortUrl) {
        System.out.println(shortUrl);
        String url = (String) session.getAttribute(shortUrl);
        try {
            response.sendRedirect(url); //将短链接映射的长链接取出并重定向到长链接
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String upLoadFile(MultipartFile file) {
        return ToolUtil.LoadFileUtil.upLoad(ToolUtil.LoadFileUtil.filePath, file);
    }

    public void redis() {
        User user = new User();
        user.setUsername("888");
        user.setPassword("aaa");
        user.setGenderStatus(5);
        System.out.println(user.toString());
        System.out.println(RedisUtil.set(user, "username", "7"));
    }

    public Object email() {
        System.out.println("DemoController.emailDemo");
        emailUtil.sendHtmlMail(emailUtil.registerTemplate("2867651844@qq.com", "123"));
        return "";
    }

    public void loginRedis() {
        User Token = new User();
        Token.setUsername("111");
        Token.setPassword("bbb");
        Token.setGenderStatus(2);
        System.out.println(Token.toString());
        System.out.println(RedisUtil.set(Token, "username", "15", RedisUtil.Time_minute));
    }
}
