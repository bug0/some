package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单抽奖记录
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OrdLuckDraw extends Model<OrdLuckDraw> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 会员手机号
     */
    private String memberMobile;

    /**
     * 会员姓名
     */
    private String memberRealName;

    /**
     * 抽奖主题
     */
    private String setLuckTitle;

    /**
     * 抽奖主体id
     */
    private Integer setLuckId;

    /**
     * 抽奖计数
     */
    private Integer setLuckCount;

    /**
     * 是否中奖 0未中奖 1中奖
     */
    private Integer isLuck;

    /**
     * 中奖人姓名
     */
    private String luckName;

    /**
     * 中奖人手机号
     */
    private String luckMobile;

    /**
     * 0未领取，1已领取
     */
    private Integer taken;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
