package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SetBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * banner设置表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetBannerInterface extends IService<SetBanner> {

}
