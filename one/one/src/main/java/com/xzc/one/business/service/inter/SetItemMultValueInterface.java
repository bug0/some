package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SetItemMultValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设置值 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetItemMultValueInterface extends IService<SetItemMultValue> {

}
