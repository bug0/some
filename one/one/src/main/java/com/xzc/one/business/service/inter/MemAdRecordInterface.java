package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAdRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员广告点击记录表 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdRecordInterface extends IService<MemAdRecord> {

}
