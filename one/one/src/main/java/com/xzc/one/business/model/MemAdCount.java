package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 广告计数
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemAdCount extends Model<MemAdCount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 获得日期
     */
    private LocalDate adDay;

    /**
     * 获得倍数
     */
    private Integer count;

    /**
     * 商家目标
     */
    private Integer producerGoal;

    /**
     * 获得广告点数
     */
    private Integer perAdPoint;

    /**
     * 0未入账，1已入账，未释放，2已释放
     */
    private Integer status;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
