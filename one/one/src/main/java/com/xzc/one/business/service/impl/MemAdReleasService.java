package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.MemAdReleas;
import com.xzc.one.business.mapper.mapper.MemAdReleasMapper;
import com.xzc.one.business.service.inter.MemAdReleasInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告点数释放记录 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class MemAdReleasService extends ServiceImpl<MemAdReleasMapper, MemAdReleas> implements MemAdReleasInterface {

}
