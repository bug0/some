package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.MemAdReleasDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 广告点击数释放详情 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdReleasDetailMapper extends BaseMapper<MemAdReleasDetail> {

}
