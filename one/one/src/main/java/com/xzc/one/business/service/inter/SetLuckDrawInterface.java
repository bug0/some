package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.SetLuckDraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 抽奖设置 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetLuckDrawInterface extends IService<SetLuckDraw> {

}
