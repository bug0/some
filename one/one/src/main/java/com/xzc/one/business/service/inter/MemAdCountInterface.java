package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemAdCount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 广告计数 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemAdCountInterface extends IService<MemAdCount> {

}
