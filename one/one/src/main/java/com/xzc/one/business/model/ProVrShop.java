package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * vr门店列表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProVrShop extends Model<ProVrShop> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 推荐人id
     */
    private Integer remMemId;

    /**
     * 门店编号
     */
    private String shopNo;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 头图
     */
    private String logo;

    /**
     * 详情图片
     */
    private String detailPic;

    /**
     * 0未上架，1已上架
     */
    private Integer status;

    /**
     * 联系人手机号
     */
    private String connectMobile;

    /**
     * 联系人名称
     */
    private String connectName;

    /**
     * 省份id
     */
    private Integer proviceId;

    /**
     * 省份名称
     */
    private String proviceName;

    /**
     * 城市id
     */
    private Integer cityId;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 区/县id
     */
    private Integer areaId;

    /**
     * 区/县名称
     */
    private String areaName;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;

    /**
     * 场地详情
     */
    private String content;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
