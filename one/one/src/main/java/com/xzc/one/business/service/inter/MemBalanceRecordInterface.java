package com.xzc.one.business.service.inter;

import com.xzc.one.business.model.MemBalanceRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包收支记录 服务类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface MemBalanceRecordInterface extends IService<MemBalanceRecord> {

}
