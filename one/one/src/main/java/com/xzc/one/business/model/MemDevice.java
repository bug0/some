package com.xzc.one.business.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 合伙人购买设备投放列表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MemDevice extends Model<MemDevice> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 合伙人id
     */
    private Integer memPartenerId;

    /**
     * 会员真实姓名
     */
    private String memberRealName;

    /**
     * 订单id
     */
    private Integer ordersId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 设备mac编号
     */
    private String proDevMac;

    /**
     * 设备供应商id
     */
    private Integer proProducerId;

    /**
     * 设备供应商名称
     */
    private String proProducerName;

    /**
     * 门店id
     */
    private Integer proShopId;

    /**
     * 门店名称
     */
    private String proShopName;

    /**
     * 门店地址
     */
    private String proShopAddress;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
