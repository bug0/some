package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.SetItems;
import com.xzc.one.business.mapper.mapper.SetItemsMapper;
import com.xzc.one.business.service.inter.SetItemsInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设置项 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class SetItemsService extends ServiceImpl<SetItemsMapper, SetItems> implements SetItemsInterface {

}
