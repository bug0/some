package com.xzc.one.business.controller;


import com.xzc.one.business.service.impl.SysUserService;
import com.xzc.one.common.inout.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 后台用户 前端控制器
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    SysUserService sysUserService;

    @RequestMapping("/sys")
    public Result sys() {
        System.out.println("SysUserController.sys");
        return Result.success(sysUserService.sys());
    }

}

