package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.OrdLuckDraw;
import com.xzc.one.business.mapper.mapper.OrdLuckDrawMapper;
import com.xzc.one.business.service.inter.OrdLuckDrawInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单抽奖记录 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class OrdLuckDrawService extends ServiceImpl<OrdLuckDrawMapper, OrdLuckDraw> implements OrdLuckDrawInterface {

}
