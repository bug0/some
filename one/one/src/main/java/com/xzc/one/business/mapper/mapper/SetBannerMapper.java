package com.xzc.one.business.mapper.mapper;

import com.xzc.one.business.model.SetBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * banner设置表 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
public interface SetBannerMapper extends BaseMapper<SetBanner> {

}
