package com.xzc.one.business.service.impl;

import com.xzc.one.business.model.OrdIncome;
import com.xzc.one.business.mapper.mapper.OrdIncomeMapper;
import com.xzc.one.business.service.inter.OrdIncomeInterface;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单收益表 服务实现类
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Service
public class OrdIncomeService extends ServiceImpl<OrdIncomeMapper, OrdIncome> implements OrdIncomeInterface {

}
