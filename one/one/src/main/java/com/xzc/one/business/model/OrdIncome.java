package com.xzc.one.business.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单收益表
 * </p>
 *
 * @author xzcong
 * @since 2019-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OrdIncome extends Model<OrdIncome> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 用户手机号
     */
    private String memberMobile;

    /**
     * 设备地址
     */
    private String devMac;

    /**
     * 设备id
     */
    private Integer devId;

    /**
     * 设备个数
     */
    private Integer devCount;

    /**
     * 设备金额
     */
    private BigDecimal devAmount;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 分润比例
     */
    private BigDecimal shareRate;

    /**
     * 订单金额（分成金额）
     */
    private BigDecimal orderAmount;

    /**
     * 收益金额
     */
    private BigDecimal incomeAmount;

    /**
     * 收益的消费点数
     */
    private BigDecimal incomeCoupon;

    /**
     * 消费点数占比
     */
    private BigDecimal couponRate;

    /**
     * 0分享扫码合伙人提成，1合伙人推荐者提成，2场地方提成，3场地推荐人提成，4区域代理人提成，5团队业绩奖励提成，6平台收益
     */
    private Integer incomeType;

    /**
     * 收益标题，说明
     */
    private String incomeTitle;

    /**
     * 0钱包，2消费点数
     */
    private Integer amountType;

    /**
     * 会员级别
     */
    private Integer memberLevel;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
