package com.xzc.one.inout;
/**
 * demo
 *
 * @author xzcong
 * @create 2019-10-20 11:49
 **/
public class Request<T> {
    private T data;             //请求参数
    private String platform;    //平台（手机、网页……）
    private String version;     //版本号

    public Request() {
    }

    public String toString() {
        return "Request{data=" + this.data + ", platform='" + this.platform + '\'' + ", version='" + this.version + '\'' + '}';
    }

    public void print() {
        System.out.println(this.toString());
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getPlatform() {
        return this.platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
