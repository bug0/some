package com.xzc.one.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xzc.one.model.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 后台用户 Mapper 接口
 * </p>
 *
 * @author xzcong
 * @since 2019-10-23
 */
@Mapper
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

}
