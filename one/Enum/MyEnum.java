package com.xzc.one.Enum;

/**
 * 枚举
 *
 * @author xzcong
 * @create 2019-10-20 13:05
 **/
public class MyEnum {

    // 性别
    public enum GenderStatus {

        Male("男", 1),
        Female("女", 0);
        private String name;
        private Integer value;

        GenderStatus(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        public static String getNameByValue(Integer value) {
            for (GenderStatus g : GenderStatus.values()) {
                if (g.getValue().equals(value)) {
                    return g.getName();
                }
            }
            return "";
        }

        public String getName() {
            return name;
        }

        public Integer getValue() {
            return value;
        }
    }

    // 禁用启用状态
    public enum EnabileStatus {

        Enable("启用", 1),
        Disable("禁用", 0);
        private String name;
        private Integer value;

        EnabileStatus(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        public static String getNameByValue(Integer value) {
            for (int i = 0; i < EnabileStatus.values().length; i++) {
                if (EnabileStatus.values()[i].equals(value)) {
                    return EnabileStatus.values()[i].getName();
                }
            }
            for (EnabileStatus e : EnabileStatus.values()) {
                if (e.getValue().equals(value)) {
                    return e.getName();
                }
            }
            return "";
        }

        public String getName() {
            return name;
        }

        public Integer getValue() {
            return value;
        }
    }
}
