package com.xzc.one.model;

import lombok.Data;

/**
 * yonghu
 *
 * @author xzcong
 * @create 2019-10-20 13:17
 **/
@Data
public class User {
    private String username;
    private String password;
    private Integer genderStatus;

    public User(String username, String password, Integer genderStatus) {
        this.username = username;
        this.password = password;
        this.genderStatus = genderStatus;
    }
}
