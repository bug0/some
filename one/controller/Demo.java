package com.xzc.one.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xzc.one.Enum.MyEnum;
import com.xzc.one.controller.Base.UserKit;
import com.xzc.one.inout.Request;
import com.xzc.one.mapper.SysUserMapper;
import com.xzc.one.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xzc.one.model.SysUser;
import com.xzc.one.model.User;
import com.xzc.one.service.impl.SysUserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * demo
 *
 * @author xzcong
 * @create 2019-10-20 11:49
 **/
@RestController
public class Demo extends UserKit {
    @Autowired
    UserMapper userMapper;
    @Autowired
    SysUserImpl sysUserService;
    @Autowired
    SysUserMapper sysUserMapper;

    @Value("${var.weixin}")
    private String weixin;

    @RequestMapping("/test")
//    public IPage<SysUser> test() {
    public void test() {
        System.out.println("Demo.test");
        Page<SysUser> page = new Page<>(1, 5);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();

        IPage<SysUser> userIPage = sysUserMapper.selectPage(page, queryWrapper);
//        return userIPage;

        System.out.println(sysUserMapper.selectById(1).toString());
//        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("id", "1");
//
//        User user = sysUserMapper.selectOne(queryWrapper);
//        System.out.println(user);
    }

    @RequestMapping("/demo")
    public String demo(@RequestBody Request<Map<String, Object>> param) {
        param.print();
        List<User> user = new LinkedList<>();
        user.add(new User("aaa", "111", 1));
        user.add(new User("bbb", "222", 0));
        for (User u : user) {
            System.out.println(MyEnum.GenderStatus.getNameByValue(Integer.valueOf(u.getGenderStatus())));
        }
        System.out.println(session.getId());
        setCookie();
        System.out.println(getCookie());
        MyEnum.GenderStatus g = MyEnum.GenderStatus.Male;
        //r.put("statusStr", MyEnum.GenderStatus.getNameByValue(Integer.valueOf(r.get("status"))));

        System.out.println(param.getData().get("username"));
        return param.toString();
    }

    @RequestMapping("/aaa")
    public String aaa(@RequestBody Request<Map<String, Object>> param) {
        System.out.println("Demo.aaa");
        return param.toString();
    }

    @RequestMapping("/bbb")
    public String bbb(String username, String password) {
        System.out.println("Demo.bbb");
        System.out.println(username);
        return username;
    }

    @RequestMapping("/sql")
    public String sql() {
        System.out.println("Demo.sql");
        System.out.println(weixin);
        System.out.println(userMapper.select());
        System.out.println(userMapper.selectUserByName(1));
        return userMapper.select();
    }

}
