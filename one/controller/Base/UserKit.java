package com.xzc.one.controller.Base;

import java.util.UUID;

/**
 * 关于用户网络请求
 *
 * @author xzcong
 * @create 2019-10-20 16:11
 **/
public class UserKit extends HttpKit {
    private final String TOKEN = "TOKEN";
    private final int MINUTE = 2 * 60;

    protected void setCookie() {
        super.setCookie(TOKEN, UUID.randomUUID().toString().replace("-", ""), MINUTE);
    }

    protected String getCookie() {
        return super.getCookie(TOKEN);
    }
}
